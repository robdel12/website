/* eslint-env node */
'use strict';

module.exports = function(/* environment, appConfig */) {
  // See https://github.com/san650/ember-web-app#documentation for a list of
  // supported properties

  return {
    name: "Robert DeLuca",
    short_name: "Robdel12",
    description: "Software developer",
    start_url: "/",
    display: "standalone",
    background_color: "#fff",
    theme_color: "#fff",
    icons: [
      {
        src: '/assets/images/icons/pwa-192x192.png',
        "sizes": "192x192",
        "type": "image/png"
      },
      {
        src: '/assets/images/icons/pwa-512x512.png',
        sizes: '512x512',
        "type": "image/png"
      }
    ]
  };
};
