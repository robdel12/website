import { Factory, faker } from 'ember-cli-mirage';

export default Factory.extend({
  url: '#?=itworked',
  title() {
    return faker.random.words();
  },
  description() {
    return faker.hacker.phrase();
  },
  publishedDate() {
    return faker.date.recent();
  }
});
