import { Factory, faker } from 'ember-cli-mirage';

export default Factory.extend({
  url: '#',
  isMedium: false,
  title() {
    return faker.random.words();
  },
  body() {
    return faker.lorem.paragraphs();
  },
  postSlug() {
    return this.title.replace(/\W/g,'-').replace(/-{1,}/g,'-').replace(/^-|-$/g,'').toLowerCase();
  },
  publishedDate() {
    return faker.date.recent();
  },
});
