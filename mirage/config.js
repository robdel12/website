export default function() {

  // These comments are here to help you get started. Feel free to delete them.

  /*
    Config (with defaults).

    Note: these only affect routes defined *after* them!
  */

  // make this `http://localhost:8080`, for example, if your API is on a different server
  this.urlPrefix = 'https://api.robertdelu.ca';

  // make this `/api`, for example, if your API is namespaced
  this.namespace = '/v1';

  // delay for each request, automatically set to 0 during testing
  // this.timing = 400;

  /*
    Shorthand cheatsheet:

    this.get('/posts');
    this.post('/posts');
    this.get('/posts/:id');
    this.put('/posts/:id'); // or this.patch
    this.del('/posts/:id');

    http://www.ember-cli-mirage.com/docs/v0.3.x/shorthands/
  */

  this.get('/timeline', (schema) => {
    return schema.timelines.all();
  });

  this.get('/post', (schema) => {
    return schema.posts.all();
  });
}
