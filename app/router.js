import EmberRouter from '@ember/routing/router';
import config from './config/environment';

const Router = EmberRouter.extend({
  location: config.locationType,
  rootURL: config.rootURL
});

Router.map(function() {
  this.route('blog', { path: 'posts' } , function() {
    this.route('show', { path: '/:postSlug' });
  });
});

export default Router;
