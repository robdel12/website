import Route from '@ember/routing/route';
import fetch from 'ember-fetch/ajax';
import ENV from 'new-frontiers/config/environment';

export default Route.extend({
  // no ember data.. means going and filtering it yo self.
  model(params) {
    return fetch(`${ENV.API}/post`).then(response => {
        let post = response.data.filter(post => {
          return post.attributes['post-slug'] === params.postSlug;
        });

        return post[0].attributes;
      });
  },

  titleToken(model) {
    return `${model.title} | Robert DeLuca`;
  }
});
