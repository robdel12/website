import Route from '@ember/routing/route';
import fetch from 'ember-fetch/ajax';
import ENV from 'new-frontiers/config/environment';

export default Route.extend({
  title: 'Robert DeLuca Blog Posts',
  model() {
    return fetch(`${ENV.API}/post`).then(response => response.data);
  }
});
