import Route from '@ember/routing/route';
import fetch from 'ember-fetch/ajax';
import ENV from 'new-frontiers/config/environment';

export default Route.extend({
  title: 'Robert DeLuca | Software Developer',
  model() {
    return fetch(`${ENV.API}/timeline?limit=15`)
      .then(response => response.data);
  }
});
