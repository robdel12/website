import { computed } from '@ember/object';
import Component from '@ember/component';

export default Component.extend({
  tagName: 'a',
  target: '_blank',
  classNames: ['spec-timeline'],
  classNameBindings: ['isInstagram'],
  attributeBindings: ['label:aria-label', 'href', 'target'],
  href: computed('url', function() {
    return this.timeline.url;
  }),
  label: computed('description', function() {
    let description = this.timeline.description || this.timeline.title;

    return `${description}`;
  }),

  iconPath: computed('type', function() {
    return `/assets/images/${this.type}.svg`;
  }),

  isInstagram: computed('image-url', function() {
    return !!this.timeline['image-url'];
  })
});
