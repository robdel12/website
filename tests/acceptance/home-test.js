import $ from 'jquery';
import {
  describe,
  it,
  beforeEach,
  afterEach
} from 'mocha';
import { expect } from 'chai';
import startApp from 'new-frontiers/tests/helpers/start-app';
import destroyApp from 'new-frontiers/tests/helpers/destroy-app';

describe('Acceptance | home', function() {
  let application;

  beforeEach(function() {
    application = startApp();
  });

  afterEach(function() {
    destroyApp(application);
  });

  describe('loading the home page', function() {
    let timeline;

    beforeEach(function() {
      timeline = server.createList('timeline', 2, {
        type: 'github'
      });
      visit('/');
    });

    it('renders two timeline cards', function() {
      expect(currentURL()).to.equal('/');
      expect($('.spec-timeline').length).to.equal(2);
    });

    it('has the correct title', function() {
      expect($('.spec-timeline:first h3').text()).to.equal(timeline[0].title);
      expect($('.spec-timeline:last h3').text()).to.equal(timeline[1].title);
    });

    it('has the correct url', function() {
      expect($('.spec-timeline:first').attr('href')).to.equal(timeline[0].url);
      expect($('.spec-timeline:first').attr('href')).to.equal(timeline[1].url);
    });

    it('has the correct aria-label', function() {
      expect($('.spec-timeline:first').attr('aria-label')).to
        .equal(timeline[0].description);
      expect($('.spec-timeline:last').attr('aria-label')).to
        .equal(timeline[1].description);
    });

    it('has the correct description', function() {
      expect($('.spec-timeline:first .spec-description').text().trim())
        .to.equal(timeline[0].description);
      expect($('.spec-timeline:last .spec-description').text().trim())
        .to.equal(timeline[1].description);
    });
  });
});
