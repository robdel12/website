import $ from 'jquery';
import moment from 'moment';
import {
  describe,
  it,
  beforeEach,
  afterEach
} from 'mocha';
import { expect } from 'chai';
import startApp from 'new-frontiers/tests/helpers/start-app';
import destroyApp from 'new-frontiers/tests/helpers/destroy-app';

describe('Acceptance | blog', function() {
  let application;

  beforeEach(function() {
    application = startApp();
  });

  afterEach(function() {
    destroyApp(application);
  });

  describe('Blog index', function() {
    let posts;

    beforeEach(function() {
      posts = server.createList('post', 8, { publishedDate: new Date('03/19/1994')});
      visit('/posts');
    });

    it('renders 8 posts', function() {
      expect(currentURL()).to.equal('/posts');
      expect($('.spec-blog-post h2').length).to.equal(8);
    });

    it('renders the post date', function() {
      let formattedDate = moment(posts[0].publishedDate).format('dddd, MMMM Do YYYY');

      expect($('.spec-blog-post:first p').text()).to.equal(formattedDate);
    });

    describe('clicking a blog post', function() {

      beforeEach(function() {
        this.firstBlog = posts[0];
        return click('.spec-blog-post:first a');
      });

      it('takes you to the blog post show', function() {
        expect(currentURL()).to.equal(`/posts/${this.firstBlog.postSlug}`);
      });

      it('has the correct title', function() {
        expect($('.spec-blog-show-header h1').text()).to.equal(this.firstBlog.title);
      });

      it('has the correct date', function() {
        let formattedDate = moment(this.firstBlog.publishedDate).format('dddd, MMMM Do YYYY');

        expect($('.spec-blog-show-header p').text()).to.equal(formattedDate);
      });
    });

  });

  describe('Visiting a medium blog post', function() {
    let post;
    beforeEach(function() {
      post = server.create('post', { isMedium: true });
      visit(`/posts/${post.postSlug}`);
    });

    it('shows medium warning', function() {
      expect($('.spec-is-medium').text()).to.contain('Heads up!');
    });

  });

});
