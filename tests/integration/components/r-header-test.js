import { expect } from 'chai';
import { describe, it } from 'mocha';
import { setupComponentTest } from 'ember-mocha';
import hbs from 'htmlbars-inline-precompile';

describe('Integration | Component | r header', function() {
  setupComponentTest('r-header', {
    integration: true
  });

  it('renders', function() {
    // Set any properties with this.set('myProperty', 'value');
    // Handle any actions with this.on('myAction', function(val) { ... });
    // Template block usage:
    // this.render(hbs`
    //   {{#r-header}}
    //     template content
    //   {{/r-header}}
    // `);

    this.render(hbs`{{r-header}}`);
    expect(this.$()).to.have.length(1);
  });
});
