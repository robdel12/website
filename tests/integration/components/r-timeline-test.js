import { expect } from 'chai';
import { describe, it } from 'mocha';
import { setupComponentTest } from 'ember-mocha';
import hbs from 'htmlbars-inline-precompile';

describe('Integration | Component | r timeline', function() {
  setupComponentTest('r-timeline', {
    integration: true
  });

  it('renders', function() {
    // Set any properties with this.set('myProperty', 'value');
    // Handle any actions with this.on('myAction', function(val) { ... });
    // Template block usage:
    // this.render(hbs`
    //   {{#r-timeline}}
    //     template content
    //   {{/r-timeline}}
    // `);
    const mockTimeline = {
      "title": "Robdel12/website-api",
      "author": "Robdel12",
      "url": "https://github.com/Robdel12/website-api/commits/59faedc144f556200e754cf913f69ed42e660e71",
      "description": "Include medium blog posts with older blog posts",
      "published-date": "2017-10-08T19:03:36Z"
    };

    this.set('mockTimeline', mockTimeline);
    this.render(hbs`{{r-timeline type="github" timeline=mockTimeline}}`);

    expect(this.$()).to.have.length(1);
    expect(this.$('h3').text()).to.equal('Robdel12/website-api');
  });
});
