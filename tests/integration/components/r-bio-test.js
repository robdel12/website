import { expect } from 'chai';
import { describe, it } from 'mocha';
import { setupComponentTest } from 'ember-mocha';
import hbs from 'htmlbars-inline-precompile';

describe('Integration | Component | r bio', function() {
  setupComponentTest('r-bio', {
    integration: true
  });

  it('renders', function() {
    // Set any properties with this.set('myProperty', 'value');
    // Handle any actions with this.on('myAction', function(val) { ... });
    // Template block usage:
    // this.render(hbs`
    //   {{#r-bio}}
    //     template content
    //   {{/r-bio}}
    // `);

    this.render(hbs`{{r-bio}}`);
    expect(this.$()).to.have.length(1);
  });
});
