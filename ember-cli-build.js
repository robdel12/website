/* eslint-env node */
'use strict';

const EmberApp = require('ember-cli/lib/broccoli/ember-app');

module.exports = function(defaults) {
  let app = new EmberApp(defaults, {
    autoprefixer: {
      browsers: ['last 2 ios version']
    },
    'asset-cache': {
      exclude: [
        'assets/images/**/*',
        'assets/fonts/**/*'
      ]
    },
    'esw-cache-first': {
      patterns: [
        '/v1/api/(.+)',
        '/assets/images/(.+)',
        '/assets/fonts/(.+)'
      ],
      version: '1'
    },
    'esw-cache-fallback': {
      patterns: [
        '/v1/api/(.+)'
      ]
    }
  });

  // Use `app.import` to add additional libraries to the generated
  // output files.
  //
  // If you need to use different assets in different
  // environments, specify an object as the first parameter. That
  // object's keys should be the environment name and the values
  // should be the asset to use in that environment.
  //
  // If the library that you are including contains AMD or ES6
  // modules that you would like to import into your application
  // please specify an object with the list of modules as keys
  // along with the exports of each module as its value.
  app.import("bower_components/highlightjs/styles/github.css");

  return app.toTree();
};
